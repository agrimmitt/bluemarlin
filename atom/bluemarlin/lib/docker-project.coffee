fs = require 'fs'
path = require 'path'

module.exports = class DockerProject
  constructor: (projectPath) ->
    @dockerfilePath = path.join(projectPath, 'Dockerfile')

  checkDockerfile: (callback) ->
    fs.stat @dockerfilePath, (err) =>
      callback err

  readProjectName: (callback) ->
    fs.readFile @dockerfilePath, {encoding:'utf8'}, (err, content) =>
      if err
        callback err
      else
        for line in content.split('\n')
          if m = line.match(/^#BMNAME +(.*)$/)
            return callback null, m[1]
        callback 'notfound', null

  readProjectPorts: (callback) ->
    publicPort = null
    privatePort = null
    fs.readFile @dockerfilePath, {encoding:'utf8'}, (err, content) =>
      if err
        callback err, null, null
      else
        for line in content.split('\n')
          if m = line.match(/^EXPOSE +(.*)$/)
            privatePort = m[1]
          else if m = line.match(/^#BMPORT +(.*)$/)
            publicPort = m[1]
        if privatePort
          callback null, privatePort, publicPort
        else
          callback 'notfound', null, null
