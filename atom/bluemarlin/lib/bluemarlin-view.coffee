{$,View} = require 'atom'
fs = require 'fs'
path = require 'path'
DockerProject = require './docker-project'
DockerConnector = require 'bluemarlin-docker-connector'
BSDeploy = require 'bluemarlin-bs-deploy'

module.exports =
class BluemarlinView extends View
  @content: ->
    @div class: 'bluemarlin xoverlay from-top', =>
      @img src: path.join(__dirname, '..', 'bluemarlinlogo.png')
      @h1 'Docker Manager'
      @a class: 'bm-close', href: '#', 'X'
      @div class: 'bm-tabs', =>
        @a class: 'project selected', 'data-tab':'project', href: '#', "Project"
        @a class: 'containers', 'data-tab':'containers', href: '#', "Containers"
        @a class: 'images', 'data-tab':'images', href: '#', "Images"
        @a class: 'deploy', 'data-tab':'deploy', href: '#', "Deploy"
        @a class: 'settings', 'data-tab':'settings', href: '#', "Settings"
      @div class: 'bm-views', =>
        @div class: 'bm-view project', =>
          @div class: 'project-content', ''
        @div class: 'bm-view containers', =>
          @table =>
            @thead =>
              @tr =>
                @th 'Name'
                @th 'Image'
                @th 'Status'
                @th ''
            @tbody class: 'container-list'
        @div class: 'bm-view images', =>
          @table =>
            @thead =>
              @tr =>
                @th 'Tag'
                @th 'Created'
                @th ''
            @tbody class: 'image-list'
        @div class: 'bm-view deploy', =>
          @div class: 'deploy-content'
        @div class: 'bm-view settings', =>
          @div class: 'settings-content', =>
            @div =>
              @h2 'Docker Settings'
            @div =>
              @label for: 'public-port', 'Public Port'
              @br()
              @input id: 'public-port', type: 'text', style: 'width:30%', placeholder: 'Port number', value: '49000'
            @div =>
              @br()
              @label for: 'docker-repository', 'Username on Docker Index'
              @br()
              @input id: 'docker-repository', type: 'text', style: 'width:30%', placeholder: 'Username', value: 'kennu'
            @div =>
              @br()
              @h2 'Keystok Settings'
            @div =>
              @label for: 'access-token', 'Keystok Access Token'
              @br()
              @input id: 'access-token', type: 'text', style: 'width:80%', placeholder: 'Access token', value: 'eyJpZCI6ODIsInJ0IjoiMzE0ZjVkZWNmNjRhMGQ4YTZjMjJjNThlNjAxNTc4MzNjYzA2YmZiYzczNzc5OTYxMzQ5NmNjNWRjNTA0ZDAzYyIsImRrIjoiYzNkYTVkNTA0YWI3ZGI2ZGQ0ZmZlZjBiMjNmZmJiMDcxYjhkOGQwZmRkMGQzNWNiNmQ1MGM1MjgyNWNlOWE4NSJ9'
            @div =>
              @br()
              @h2 'Amazon Beanstalk Settings'
            @div =>
              @label for: 'bs-application', 'Application name'
              @br()
              @input id: 'bs-application', type: 'text', style: 'width:50%', placeholder: 'Application name', value: 'TestApplication'
            @div =>
              @br()
              @label for: 'bs-environment', 'Environment name'
              @br()
              @input id: 'bs-environment', type: 'text', style: 'width:50%', placeholder: 'Environment name', value: 'TestEnvironment'
            @div =>
              @br()
              @label for: 'bs-bucket', 'S3 bucket name'
              @br()
              @input id: 'bs-bucket', type: 'text', style: 'width:50%', placeholder: 'Bucket name', value: 'bluemarlintest'
            @div =>
              @br()
            @div =>
              @button class: 'button', 'Save Changes'

  initialize: (serializeState) ->
    atom.workspaceView.command "bluemarlin:show-docker-manager", => @toggleDockerManager()
    @dockerProject = new DockerProject(atom.project.getPath())
    @dockerConnector = new DockerConnector
      host: 'http://127.0.0.1'
      port: 2375
    @find('.bm-views .bm-view').hide()
    @find('.bm-views .bm-view.project').show()
    @building = false
    @currentLog = ''
    @currentDeployLog = ''

  # Returns an object that can be retrieved when package is activated
  serialize: ->

  # Tear down any state and detach
  destroy: ->
    @detach()

  toggleDockerManager: ->
    if @hasParent()
      @detach()
    else
      atom.workspaceView.appendToRight(this)
      @refreshContent 'project'

  setupEvents: ->
    @find('.bm-close').unbind('click').bind 'click', =>
      @detach()
    @find('.bm-tabs a').unbind('click').bind 'click', (event) =>
      tab = $(event.target).attr('data-tab')
      @find('.bm-tabs a').removeClass 'selected'
      $(event.target).addClass 'selected'
      @find('.bm-views .bm-view').hide()
      @find('.bm-views .bm-view.' + tab).show()
      @refreshContent tab

  refreshContent: (tab) ->
    if tab == 'project'
      @refreshProject()
    else if tab == 'containers'
      @refreshContainers()
    else if tab == 'images'
      @refreshImages()
    else if tab == 'deploy'
      @refreshDeploy()
    else if tab == 'settings'
      @refreshSettings()
    @setupEvents()

  refreshProject: ->
    content = @find('.bm-views .bm-view.project .project-content')
    @dockerProject.readProjectName (err, projectName) =>
      if err
        content.html '<p>This project does not have a Dockerfile yet</p>' +
          '<p><button class="autogenerate button">Autogenerate</button></p>'
        @find('.bm-views .bm-view.project button.autogenerate').unbind('click').bind 'click', (event) =>
          @autogenerateProject()
      else
        @projectName = projectName
        @containerName = projectName
        p = @containerName.lastIndexOf('/')
        if p >= 0
          @containerName = @containerName.slice(p+1)
        @dockerConnector.imageExists @projectName + ':latest', (err) =>
          @imageExists = not err
          @dockerConnector.containerExists @containerName, (err) =>
            @running = not err
            @projectStatus = if @building then 'Building' else if @running then 'Running' else if @imageExists then 'Image OK' else 'Not built yet'
            @autogenerateEnabled = not @building
            @buildEnabled = true
            @runEnabled = not @building and @imageExists
            @deployEnabled = not @building and @imageExist
            @pushEnabled = not @building and @imageExist
            content.html '<table><tbody>' +
              '<tr><td>Project name: ' + @projectName + '</td></tr>' +
              '<tr><td>Project status: ' + @projectStatus + '</td></tr>' +
              '</table>' +
              '<br>' +
              '<p>' +
              '<button class="autogenerate button">Regenerate</button> ' +
              '<button class="build button">Build</button> ' +
              '<button class="run button">Start</button> ' +
              '</p>' +
              '<br>' +
              '<div class="log">' + @currentLog + '</div>'
            @updateButtons()
            div = @find('.log')
            div.scrollTop div.prop("scrollHeight")
            @find('.bm-views .bm-view.project button.autogenerate').unbind('click').bind 'click', (event) =>
              @autogenerateProject()
            @find('.bm-views .bm-view.project button.build').unbind('click').bind 'click', (event) =>
              @buildProject()
            @find('.bm-views .bm-view.project button.run').unbind('click').bind 'click', (event) =>
              @runProject()

  updateButtons: ->
    @find('button.autogenerate').prop('disabled', not @autogenerateEnabled).html 'Regenerate'
    @find('button.build').prop('disabled', not @buildEnabled).html (if @building then 'Stop build' else if @imageExists then 'Rebuild' else 'Build')
    @find('button.run').prop('disabled', not @runEnabled).html (if @running then 'Stop' else 'Start')
    @find('button.deploy').prop('disabled', not @deployEnabled).html 'Deploy to Amazon Beanstalk'
    @find('button.push').prop('disabled', not @pushEnabled).html 'Push to Docker Index'

  refreshContainers: ->
    tbody = @find('.bm-views .bm-view.containers .container-list')
    tbody.html('')
    @dockerConnector.listContainers (err, containers) =>
      console.log 'Containers:', containers
      if containers
        for container in containers
          name = container.Names[0]
          if name.slice(0, 1) == '/'
            name = name.slice(1)
          tbody.append '<tr>' +
            '<td>' + name + '</td>' +
            '<td>' + container.Image + '</td>' +
            '<td>' + container.Status + '</td>' +
            '<td><a href="#" class="stop" data-id="' + container.Id + '">Stop</a></td>' +
            '</tr>'
      @find('.bm-views .bm-view.containers a.stop').unbind('click').bind 'click', (event) =>
        id = $(event.target).attr('data-id')
        console.log 'Stopping', id
        container = @dockerConnector.getContainer(id)
        container.stop (err) =>
          console.log err
          container.remove (err) =>
            console.log err
            @refreshContainers()

  formatDate: (date) ->
    date.toString().slice(0, 15)

  refreshImages: ->
    tbody = @find('.bm-views .bm-view.images .image-list')
    tbody.html('')
    @dockerConnector.listImages (err, images) =>
      if images
        for image in images
          if image.RepoTags[0] != '<none>:<none>'
            tbody.append '<tr>' +
              '<td>' + image.RepoTags[0].replace(/</g, '&lt;') + '</td>' +
              '<td>' + @formatDate(new Date(image.Created*1000)) + '</td>' +
              '<td><a href="#" class="remove" data-id="' + image.Id + '">Remove</a></td>' +
              '</tr>'
      @find('.bm-views .bm-view.images a.remove').unbind('click').bind 'click', (event) =>
        id = $(event.target).attr('data-id')
        console.log 'Removing', id
        image = @dockerConnector.getImage(id)
        image.remove (err) =>
          console.log err
          @refreshImages()

  refreshDeploy: ->
    content = @find('.bm-views .bm-view.deploy .deploy-content')
    content.html '<p>' +
      '<button href="#" class="deploy button">Deploy to Amazon Beanstalk</button> ' +
      '<button href="#" class="push button">Push to Docker Index</button> ' +
      '</p><div class="log">' + @currentDeployLog + '</div>'
    @find('.bm-views .bm-view.deploy button.deploy').unbind('click').bind 'click', (event) =>
      @deployProject()
    @find('.bm-views .bm-view.deploy button.push').unbind('click').bind 'click', (event) =>
      @pushProject()

  refreshSettings: ->
    content = @find('.bm-views .bm-view.settings .settings-content')

  clearLog: ->
    @currentLog = ''
    @find('.bm-view.project .log').html ''

  log: (msg) ->
    div = @find('.bm-view.project .log')
    div.append '<div>' + msg + '</div>'
    @currentLog += '<div>' + msg + '</div>'
    div.scrollTop(div.prop("scrollHeight"));

  clearDeployLog: ->
    @currentDeployLog = ''
    @find('.bm-view.deploy .log').html ''

  deployLog: (msg) ->
    div = @find('.bm-view.deploy .log')
    div.append '<div>' + msg + '</div>'
    @currentDeployLog += '<div>' + msg + '</div>'
    div.scrollTop(div.prop("scrollHeight"));

  autogenerateProject: ->
    autoconvert = require('bluemarlin-docker-generator')
    @clearLog()
    @log 'Autogenerating Dockerfile...'
    autoconvert atom.project.getPath(), @find('#docker-repository').val(), true, (err) =>
      if err
        @log 'Autogeneration failed: ' + err
      else
        @log 'Autogeneration completed successfully.'
      @refreshProject()

  buildProject: ->
    if @building
      if @buildResponse
        @buildResponse.socket.destroy()
      return
    @clearLog()
    @log 'Building ' + @projectName
    @building = true
    @find('button.build').html 'Stop build'
    @dockerConnector.buildImage atom.project.getPath(), @projectName, (err, response) =>
      #console.log 'Build status', err, response
      @buildResponse = response
      response.on 'data', (chunk) =>
        try
          data = JSON.parse(chunk.toString('utf8'))
          if data.stream
            @log data.stream
          else if data.errorDetail
            @log data.errorDetail.message
          else
            console.log data
        catch err
          # JSON error
      response.on 'end', =>
        @log 'Build finished.'
        @building = false
        @buildResponse = null
        @refreshProject()

  runProject: ->
    @clearLog()
    if @running
      @dockerConnector.getContainerByName @containerName, (err, container) =>
        if err
          @log 'Error stopping container: ' + err.toString()
        else
          @log 'Stopping container...'
          container.stop (err) =>
            if err
              @log 'Error stopping container: ' + err.toString()
            else
              container.remove (err) =>
                if err
                  @log 'Error removing container: ' + err.toString()
                else
                  @log 'Container stopped successfully.'
                @refreshProject()
      return
    @dockerProject.readProjectPorts (err, privatePort) =>
      publicPort = @find('#public-port').val()
      if err
        @log 'Error reading exposed ports'
      else
        @log 'Running container at port ' + publicPort + ':' + privatePort
        @dockerConnector.runContainer @containerName, @projectName, publicPort, privatePort, 'KEYSTOK_TOKEN=' + @find('#access-token').val(), (err, data, container) =>
          if err
            @log 'Failed to start container: ' + err.toString()
          else
            @log 'Container started successfully.'
            @log 'Open in browser: <a target="_blank" href="http://localhost:' + publicPort + '">http://localhost:' + publicPort + '</a>'
          @refreshProject()

  deployProject: ->
    @clearDeployLog()
    @deployLog 'Deploying to Amazon Beanstalk...'
    token = @find('#access-token').val()
    bs = new BSDeploy
      'keystokAccessToken': token + '===='
      'sourcePath': atom.project.getPath()
      'appName': @find('#bs-application').val()
      'environmentName': @find('#bs-environment').val()
      'bucketName': @find('#bs-bucket').val()
      'log': (msg) => @deployLog msg
    bs.deploy {}, (err) =>
      if err
        @deployLog 'Deployment failed: ' + err
      else
        @deployLog 'Deployment successful.'

  pushProject: ->
    @clearDeployLog()
    @deployLog 'Pushing to Docker Index...'
    @dockerConnector.pushImage @projectName, (err, response) =>
      if err
        @deployLog 'Push failed: ' + err
      else
        response.on 'data', (chunk) =>
          try
            data = JSON.parse(chunk.toString('utf8'))
            if data.stream
              @deployLog data.stream
            else if data.status
              @deployLog data.status
            else if data.errorDetail
              @deployLog data.errorDetail.message
            else
              console.log data
          catch err
            # JSON error
        response.on 'end', =>
          @deployLog 'Push finished.'
