{WorkspaceView} = require 'atom'
Bluemarlin = require '../lib/bluemarlin'

# Use the command `window:run-package-specs` (cmd-alt-ctrl-p) to run specs.
#
# To run a specific `it` or `describe` block add an `f` to the front (e.g. `fit`
# or `fdescribe`). Remove the `f` to unfocus the block.

describe "Bluemarlin", ->
  activationPromise = null

  beforeEach ->
    atom.workspaceView = new WorkspaceView
    activationPromise = atom.packages.activatePackage('bluemarlin')

  describe "when the bluemarlin:toggle event is triggered", ->
    it "attaches and then detaches the view", ->
      expect(atom.workspaceView.find('.bluemarlin')).not.toExist()

      # This is an activation event, triggering it will cause the package to be
      # activated.
      atom.workspaceView.trigger 'bluemarlin:toggle'

      waitsForPromise ->
        activationPromise

      runs ->
        expect(atom.workspaceView.find('.bluemarlin')).toExist()
        atom.workspaceView.trigger 'bluemarlin:toggle'
        expect(atom.workspaceView.find('.bluemarlin')).not.toExist()
