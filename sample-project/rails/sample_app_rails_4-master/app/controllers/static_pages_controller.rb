class StaticPagesController < ApplicationController

  def home
    @secret_string = Rails.application.secrets.secret_string
    if signed_in?
      @micropost  = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    end
  end

  def help
  end

  def about
  end

  def contact
  end
end
