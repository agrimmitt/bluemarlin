#!/usr/bin/env node

var program = require('commander');

program
  .version('0.1.0')
  .option('-f, --force', 'Force overwrite of existing Dockerfile(s)')
  .parse(process.argv);

program.on('--help', function() {
  console.log('  Commands:');
  console.log('');
  console.log('    bluemarlin.js autogenerate [railspath] ... To autoconvert Rails project to Dockerfile');
  console.log('');
});

var command = program.args[0];
var path = program.args[1];

if (command == 'autogenerate' && path) {
  require('bluemarlin-docker-generator')(path, program.force);
} else {
  program.help();
}
