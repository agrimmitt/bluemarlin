#!/usr/bin/env ruby

require 'keystok'
require 'yaml'

RAILS_DIR = File.expand_path(ARGV[0] || '.')

keystok = Keystok::Client.new(ENV['KEYSTOK_TOKEN'])

[%w(config secrets.yml), %w(config database.yml)].each do |path|
  filepath = File.join(RAILS_DIR, *path)
  unless File.exists?(filepath)
    next
  end
  config_file = YAML.load_file(filepath)
  config_file.each do |env, data|
    data.each do |key, value|
      if value == 'KEYSTOK_FIX_ME'
        keystok_id = "#{File.basename(path.last, '.*')}-#{env}-#{key}"
        if keystok.get(keystok_id)
          config_file[env][key] = keystok.get(keystok_id)
        end
      end
    end
  end
  File.open(filepath, 'w') { |file| file.write(config_file.to_yaml) }
end
